# ---------------------------------
# Docker's metainformations:
# ---------------------------------
FROM jocelynlopez/octave
MAINTAINER Jocelyn LOPEZ <jocelyn.lopez.pro@gmail.com>

# ---------------------------------
# Install required ubuntu packages:
# ---------------------------------
RUN apt-get update && apt-get install -y --no-install-recommends \
        libpq-dev \
        postgresql \
        postgresql-contrib \
        libjpeg62-turbo-dev \
&& rm -rf /var/lib/apt/lists/*

# ---------------------------------
# Install required python packages:
# ---------------------------------
ADD requirements/ /requirements/
RUN pip install -U pip && pip install -r /requirements/production.txt -r /requirements/calculations.txt

# ---------------------------------
# Environment variables:
# ---------------------------------
# Add custom environment variables needed by Django or your settings file here:
ENV DJANGO_SETTINGS_MODULE=scienticod.settings.production DJANGO_DEBUG=off
# uWSGI configuration (customize as needed):
ENV UWSGI_WSGI_FILE=scienticod/wsgi_production.py UWSGI_HTTP=:8000 UWSGI_MASTER=1 UWSGI_WORKERS=2 UWSGI_THREADS=8 UWSGI_UID=1000 UWSGI_GID=2000

# ---------------------------------
# Add source files:
# ---------------------------------
ADD . /code/

# ---------------------------------
# Configuration:
# ---------------------------------
EXPOSE 8000
WORKDIR /code/
# make sure static files are writable by uWSGI process
RUN chown -R 1000:2000 /code/scienticod/media
# Call collectstatic with dummy environment variables:
RUN DATABASE_URL=postgres://none REDIS_URL=none python manage.py collectstatic --noinput

# ---------------------------------
# Entrypoint:
# ---------------------------------
# start uWSGI, using a wrapper script to allow us to easily add more commands to container startup:
ENTRYPOINT ["/code/docker-entrypoint.sh"]
CMD ["uwsgi", "--http-auto-chunked", "--http-keepalive", "--static-map", "/media/=/code/scienticod/media/"]
