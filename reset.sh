#!/usr/bin/env bash

## Delete previous database
#rm scienticoddb
#
## Re-made database
#/home/jocelyn/PycharmProjects/virtualenvs/scienticod/bin/python3.6 manage.py makemigrations calculations base blog
#/home/jocelyn/PycharmProjects/virtualenvs/scienticod/bin/python3.6 manage.py migrate
#/home/jocelyn/PycharmProjects/virtualenvs/scienticod/bin/python3.6 manage.py load_initial_data
#
##./manage.py dumpdata auth.user --indent 2 > user.json
#./manage.py dumpdata --natural-foreign --indent 2 -e contenttypes -e wagtailcore.GroupCollectionPermission -e wagtailimages.rendition -e sessions > scienticod/base/fixtures/scienticod_v1.json
#
#./manage.py dumpdata --indent 2 > scienticod/base/fixtures/scienticod_v1.json

docker-compose down
docker-compose build
docker-compose up -d
sleep 15
docker-compose logs

# Dump database
#./manage.py dumpdata --natural-foreign --indent 2 -e auth.permission -e contenttypes -e wagtailcore.GroupCollectionPermission -e wagtailimages.rendition -e sessions > scienticod/base/fixtures/scienticod_v1.json