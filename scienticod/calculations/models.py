import json

from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User
from django.shortcuts import render
from django.conf import settings

from taggit.models import TaggedItemBase

from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager

from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailforms.models import AbstractForm, AbstractFormField, AbstractFormSubmission
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from scienticod.calculations.forms import CalculationFormBuilder
from scienticod.calculations.lib import LIB_CALCULATIONS_FUNCTION, LIB_CALCULATIONS_NAME
from scienticod.base.blocks import BaseStreamBlock


class CalculationPageTag(TaggedItemBase):
    content_object = ParentalKey('CalculationPage', related_name='tagged_items')


class CalculationPage(AbstractForm):
    """
    Detail view for a specific calculation
    """
    parent_page_types = ['CalculationsIndexPage']
    form_builder = CalculationFormBuilder

    description = models.TextField(
        help_text='Text to describe the calculation',
        blank=False)
    applicability = StreamField(
        BaseStreamBlock(), verbose_name="Applicability", blank=True
    )
    reference = StreamField(
        BaseStreamBlock(), verbose_name="Reference", blank=True
    )
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Landscape mode only; horizontal width between 1000px and 3000px.'
    )
    tags = ClusterTaggableManager(through=CalculationPageTag, blank=True)

    content_panels = AbstractForm.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('description', classname="full"),
                StreamFieldPanel('applicability', classname="full"),
                StreamFieldPanel('reference', classname="full"),
            ],
            heading="Description",
            classname="collapsible collapsed"
        ),
    ]

    promote_panels = Page.promote_panels + [
        ImageChooserPanel('image'),
        FieldPanel('tags'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('description'),
        index.SearchField('applicability'),
        index.SearchField('reference'),
        index.SearchField('description'),
        index.FilterField('tags'),
    ]

    preview_modes = [
        ('form', 'Form'),
        ('landing', 'Landing page'),
    ]

    def get_submission_class(self):
        return CalculationSubmission

    def compute(self, cleaned_data):
        raise NotImplementedError("This method need to be defined by a subclass")

    def process_form_submission(self, form):
        from datetime import datetime
        start_time = datetime.now()
        results = self.compute(form.cleaned_data)
        cpu_time = datetime.now() - start_time
        return self.get_submission_class().objects.create(
            form_data=json.dumps(form.cleaned_data, cls=DjangoJSONEncoder),
            page=self, results=results, cpu_time=cpu_time,
            #user=form.user
        )

    def serve(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = self.get_form(request.POST, request.FILES, page=self, user=request.user)

            if form.is_valid():
                self.process_form_submission(form)
                # render the landing_page
                # TODO: It is much better to redirect to it
                calculation_submitted = self.get_submission_class().objects.filter(
                    form_data=json.dumps(form.cleaned_data, cls=DjangoJSONEncoder), page=self).first()
                landing_context = self.get_context(request)
                landing_context.update({
                    "results": calculation_submitted.results
                })
                return render(
                    request,
                    self.get_landing_page_template(request),
                    landing_context
                )
        else:
            form = self.get_form(page=self, user=request.user)

        context = self.get_context(request)
        context['form'] = form
        return render(
            request,
            self.get_template(request),
            context
        )


class OctaveCalculationPage(CalculationPage):
    template = "calculations/calculation_page.html"
    script = models.TextField(
        help_text='Octave script',
        blank=True)

    content_panels = CalculationPage.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('script', classname="full"),
                InlinePanel('form_fields', label="Variables"),
            ],
            heading="Definition",
            classname="collapsible collapsed"
        ),
    ]

    def compute(self, cleaned_data):
        from oct2py import octave
        octave.eval(self.script)
        res = octave.main(cleaned_data)
        return json.dumps(res, cls=DjangoJSONEncoder)


class PythonCalculationPage(CalculationPage):
    template = "calculations/calculation_page.html"
    function = models.CharField(
        help_text="Python function",
        blank=False,
        max_length=255,
        choices=LIB_CALCULATIONS_NAME,
    )

    content_panels = CalculationPage.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('function', classname="full"),
                InlinePanel('form_fields', label="Variables"),
            ],
            heading="Definition",
            classname="collapsible collapsed"
        ),
    ]

    def compute(self, cleaned_data):
        res = LIB_CALCULATIONS_FUNCTION[self.function](**cleaned_data)
        return json.dumps(res, cls=DjangoJSONEncoder)


class CalculationSubmission(AbstractFormSubmission):
    """
    Detail view for a specific calculation results
    """
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    # TODO: Make user account app
    cpu_time = models.DurationField(null=True, blank=True)
    results = models.TextField()

    search_fields = Page.search_fields + [
        # index.FilterField('user'),
        index.FilterField('cpu_time'),
        index.FilterField('results'),
    ]


class CalculationsIndexPage(Page):
    """
    Index page for calculations.

    This is more complex than other index pages on the bakery demo site as we've
    included pagination. We've separated the different aspects of the index page
    to be discrete functions to make it easier to follow
    """
    subpage_types = ['CalculationsIndexPage', 'OctaveCalculationPage',
                     'PythonCalculationPage']

    introduction = models.TextField(
        help_text='Text to describe the page',
        blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Landscape mode only; horizontal width between 1000px and '
        '3000px.'
    )

    content_panels = Page.content_panels + [
        FieldPanel('introduction', classname="full"),
        ImageChooserPanel('image'),
    ]

    # Returns a queryset of BreadPage objects that are live, that are direct
    # descendants of this index page with most recent first
    def get_calculations(self):
        return CalculationPage.objects.live().descendant_of(
            self).order_by('-first_published_at')

    # Allows child objects (e.g. BreadPage objects) to be accessible via the
    # template. We use this on the HomePage to display child items of featured
    # content
    def children(self):
        return self.get_children().specific().live()

    # Pagination for the index page. We use the `django.core.paginator` as any
    # standard Django app would, but the difference here being we have it as a
    # method on the model rather than within a view function
    def paginate(self, request, *args):
        page = request.GET.get('page')
        paginator = Paginator(self.get_calculations(), 12)
        try:
            pages = paginator.page(page)
        except PageNotAnInteger:
            pages = paginator.page(1)
        except EmptyPage:
            pages = paginator.page(paginator.num_pages)
        return pages

    # Returns the above to the get_context method that is used to populate the
    # template
    def get_context(self, request):
        context = super(CalculationsIndexPage, self).get_context(request)

        # CalculationPage objects (get_calculations) are passed through pagination
        calculations = self.paginate(request, self.get_calculations())

        context['calculations'] = calculations

        return context
