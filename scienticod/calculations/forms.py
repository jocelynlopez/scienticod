from django.db import models
from django.forms import IntegerField, FloatField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext_lazy as _

from modelcluster.fields import ParentalKey

from wagtail.wagtailforms.forms import FormBuilder
from wagtail.wagtailforms.models import AbstractFormField, AbstractFormSubmission
from wagtail.wagtailadmin.edit_handlers import FieldPanel


class CalculationFormBuilder(FormBuilder):
    def get_field_options(self, field):
        options = super(CalculationFormBuilder, self).get_field_options(field)
        validators = []
        if field.max_value:
            validators.append(MaxValueValidator(field.max_value))
        if field.min_value:
            validators.append(MinValueValidator(field.min_value))
        options.update({"validators": validators})
        return options

    def create_integer_field(self, field, options):
        return IntegerField(**options)

    def create_float_field(self, field, options):
        return FloatField(**options)

    FIELD_TYPES = FormBuilder.FIELD_TYPES
    FIELD_TYPES.update({
        'integer': create_integer_field,
        'float': create_float_field,
})


class CalculationFormField(AbstractFormField):
    page = ParentalKey('CalculationPage', related_name='form_fields')

    FORM_FIELD_CHOICES = (
        ('integer', _('Whole number')),
        ('float', _('Float number')),
        ('checkbox', _('Boolean')),
        ('dropdown', _('Unique choice using Dropdown')),
        ('radio', _('Unique choice using Radio buttons')),
        ('checkboxes', _('Multi choices using Checkboxes')),
        ('singleline', _('Single line text')),
        # ('multiline', _('Multi-line text')),
        # ('multiselect', _('Multiple select')),
    )

    field_type = models.CharField(
        verbose_name=_('field type'),
        max_length=16,
        choices=FORM_FIELD_CHOICES)

    max_value = models.FloatField(
        verbose_name=_('maximal value'),
        max_length=255,
        blank=True, null=True,
        help_text=_('Maximal value.')
    )

    min_value = models.FloatField(
        verbose_name=_('minimal value'),
        max_length=255,
        blank=True, null=True,
        help_text=_('Minimal value.')
    )

    panels = [
        FieldPanel('label'),
        FieldPanel('help_text'),
        FieldPanel('required'),
        FieldPanel('field_type', classname="formbuilder-type"),
        FieldPanel('choices', classname="formbuilder-choices"),
        FieldPanel('default_value', classname="formbuilder-default"),
        FieldPanel('max_value'),
        FieldPanel('min_value'),
    ]