from unit_converter.converter import converts

LIB_CALCULATIONS_FUNCTION = {
    "unit_converter.converter.converts": converts
}

LIB_CALCULATIONS_NAME = (
    ("unit_converter.converter.converts", "unit_converter.converter.converts(quantity: str, desired_unit: str) -> str"),
)
